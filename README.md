Stepper Motor Controller v2.1 2017
==========================================

Features:
- TMC2660 stepper motor driver
- LPC11U24 with USB
- ESP8266 socket
- temp sensor
- 2 endstop connector

![Rendering][rendering]

[rendering]: ./Integrated_motor_controller/steppr_controller.jpg



